const path = require("path");
import v4 from 'uuid';
import fs from 'fs';
import CartParser from './CartParser';

const uuidString = 'uuid';
let parser;

jest.mock('uuid');
jest.mock('fs');


beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	describe('validate', () => {
		test('should not receive any errors with valid data', () => {
			const contents =
				`Product name,Price,Quantity
				 Mollis consequat,9.00,2
				 Tvoluptatem,10.32,1`;

			const errors = parser.validate(contents);

			expect(errors).toHaveLength(0);
		});

		test('should receive an error when header name doesn\'t correspond to the scheme', () => {
			const wrongColumnName = 'NotPrice';
			const contents =
				`Product name,${wrongColumnName},Quantity
				 Mollis consequat,9.00,2
				 Tvoluptatem,10.32,1`;
			const expectedName = parser.schema.columns[1].name;
			const expectedErrors = [{
				type: parser.ErrorType.HEADER,
				row: 0,
				column: 1,
				message: `Expected header to be named "${expectedName}" but received ${wrongColumnName}.`
			}];

			const resultErrors = parser.validate(contents);

			expect(resultErrors).toEqual(expectedErrors);
		});

		test('should receive an error when row length doesn\'t correspond to the scheme', () => {
			const contents =
				`Product name,Price,Quantity
				 Mollis consequat
				 Tvoluptatem,10.32,1`;
			const actualLength = 1;
			const expectedLength = parser.schema.columns.length;
			const expectedErrors = [{
				type: parser.ErrorType.ROW,
				row: 1,
				column: -1,
				message: `Expected row to have ${expectedLength} cells but received ${actualLength}.`
			}];

			const resultErrors = parser.validate(contents);

			expect(resultErrors).toEqual(expectedErrors);
		});

		test('should receive an error when receiving empty string instead of a nonempty string', () => {
			const actualValue = '';
			const contents =
				`Product name,Price,Quantity
				 ${actualValue},9.00,2
				 Tvoluptatem,10.32,1`;
			const expectedErrors = [{
				type: parser.ErrorType.CELL,
				row: 1,
				column: 0,
				message: `Expected cell to be a nonempty string but received "${actualValue}".`
			}];

			const resultErrors = parser.validate(contents);

			expect(resultErrors).toEqual(expectedErrors);
		});

		test('should receive an error when expecting a positive number value but receiving string value', () => {
			const actualValue = 'string';
			const contents =
				`Product name,Price,Quantity
				 Mollis consequat,${actualValue},2
				 Tvoluptatem,10.32,1`;
			const expectedErrors = [{
				type: parser.ErrorType.CELL,
				row: 1,
				column: 1,
				message: `Expected cell to be a positive number but received "${actualValue}".`
			}];

			const resultErrors = parser.validate(contents);

			expect(resultErrors).toEqual(expectedErrors);
		});

		test('should receive an error when expecting a positive number value but receiving a negative number', () => {
			const actualValue = '-1';
			const contents =
				`Product name,Price,Quantity
				 Mollis consequat,${actualValue},2
				 Tvoluptatem,10.32,1`;
			const expectedErrors = [{
				type: parser.ErrorType.CELL,
				row: 1,
				column: 1,
				message: `Expected cell to be a positive number but received "${actualValue}".`
			}];

			const resultErrors = parser.validate(contents);

			expect(resultErrors).toEqual(expectedErrors);
		});

		test('should receive multiple errors when given input has multiple issues', () => {
			const wrongColumnName = 'NotPrice';
			const actualValue = '-1';
			const expectedColumnName = parser.schema.columns[1].name;
			const contents =
				`Product name,${wrongColumnName},Quantity
				 Mollis consequat,${actualValue},2
				 Tvoluptatem,10.32,1`;
			const expectedErrors = [
				{
					type: parser.ErrorType.HEADER,
					row: 0,
					column: 1,
					message: `Expected header to be named "${expectedColumnName}" but received ${wrongColumnName}.`
				},
				{
					type: parser.ErrorType.CELL,
					row: 1,
					column: 1,
					message: `Expected cell to be a positive number but received "${actualValue}".`
				}
			];

			const resultErrors = parser.validate(contents);

			expect(resultErrors).toEqual(expectedErrors);
		});
	});

	describe('parseLine', () => {
		beforeAll(() => {
			v4.mockReturnValue(uuidString);
		});

		test('should parse given line correctly', () => {
			const
				name = 'Tvoluptatem',
				price = 10.32,
				quantity = 1;
			const csvLine = `${name},${price},${quantity}`;

			const item = parser.parseLine(csvLine);

			expect(item).toEqual({
				id: uuidString,
				name,
				price,
				quantity
			});
		});
	});

	describe('calcTotal', () => {
		test('should return correct total price', () => {
			const cartItems = [
				{
					"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
					"name": "Mollis consequat",
					"price": 9,
					"quantity": 2
				},
				{
					"id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
					"name": "Tvoluptatem",
					"price": 10.32,
					"quantity": 1
				},
				{
					"id": "33c14844-8cae-4acd-91ed-6209a6c0bc31",
					"name": "Scelerisque lacinia",
					"price": 18.9,
					"quantity": 1
				}
			];

			const totalPrice = parser.calcTotal(cartItems);

			expect(totalPrice).toBeCloseTo(9 * 2 + 10.32 + 18.9);
		});
	});

	describe('parse', () => {
		beforeAll(() => {
			v4.mockReturnValue(uuidString);
		});

		test('should parse correctly when given correct data', () => {
			const contents =
				`Product name,Price,Quantity
				 Mollis consequat,9.00,2
				 Tvoluptatem,10.32,1`;
			fs.readFileSync.mockReturnValueOnce(contents);

			const result = parser.parse();

			expect(result).toEqual({
				"items": [
					{
						"id": uuidString,
						"name": "Mollis consequat",
						"price": 9,
						"quantity": 2
					},
					{
						"id": uuidString,
						"name": "Tvoluptatem",
						"price": 10.32,
						"quantity": 1
					}
				],
				"total": 28.32
			});
		});

		test('should throw error when given incorrect data', () => {
			const wrongColumnName = 'NotPrice';
			const contents =
				`Product name,${wrongColumnName},Quantity
				 Mollis consequat,9.00,2
				 Tvoluptatem,10.32,1`;
			fs.readFileSync.mockReturnValueOnce(contents);

			expect(() => parser.parse()).toThrow('Validation failed!');
		});
	})
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	beforeAll(() => {
		v4.mockReturnValue(uuidString);
		fs.readFileSync.mockImplementationOnce(require.requireActual('fs').readFileSync); // "unmock" readFileSync function
	});

	test('should parse correctly when given correct data', () => {
		const result = parser.parse(path.resolve(__dirname, '../samples/cart.csv'));

		expect(result).toEqual({
			"items": [
				{
					"id": uuidString,
					"name": "Mollis consequat",
					"price": 9,
					"quantity": 2
				},
				{
					"id": uuidString,
					"name": "Tvoluptatem",
					"price": 10.32,
					"quantity": 1
				},
				{
					"id": uuidString,
					"name": "Scelerisque lacinia",
					"price": 18.9,
					"quantity": 1
				},
				{
					"id": uuidString,
					"name": "Consectetur adipiscing",
					"price": 28.72,
					"quantity": 10
				},
				{
					"id": uuidString,
					"name": "Condimentum aliquet",
					"price": 13.9,
					"quantity": 1
				}
			],
			"total": 348.32
		});
	});
});